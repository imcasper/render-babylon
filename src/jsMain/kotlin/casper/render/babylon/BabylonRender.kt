package casper.render.babylon

import BABYLON.*
import BABYLON.extension.runRenderLoop
import casper.geometry.Matrix4d
import casper.geometry.Transform
import casper.geometry.Vector2i
import casper.geometry.Vector3d
import casper.render.Camera
import casper.render.Environment
import casper.render.Light
import casper.render.Render
import casper.render.model.SceneNode
import casper.types.BLACK
import casper.types.setAlpha
import casper.util.Inspector
import casper.util.toColor4
import casper.util.toMatrix4d
import casper.util.toVector3
import org.w3c.dom.HTMLCanvasElement
import kotlin.browser.document
import kotlin.browser.window

class BabylonCamera(val nativeCamera: TargetCamera, val onViewport: () -> Vector2i) : Camera {
	override val projection: Matrix4d
		get() = nativeCamera.getProjectionMatrix().toMatrix4d()
	override val view: Matrix4d
		get() = nativeCamera.getViewMatrix().toMatrix4d()
	override val viewProjection: Matrix4d
		get() = nativeCamera.getTransformationMatrix().toMatrix4d()
	override val viewport: Vector2i
		get() = onViewport()

	override var transform: Transform = Transform.IDENTITY
		set(value) {
			if (field == value) return
			field = value
			nativeCamera.position = value.position.toVector3()
			nativeCamera.setTarget((value.position + value.rotation.transform(Vector3d.Y)).toVector3())
			nativeCamera.upVector = value.rotation.transform(Vector3d.Z).toVector3()
		}

}

class BabylonRender(val canvas: HTMLCanvasElement, val settings: BabylonRenderSettings) : Render {
	val nativeEngine = Engine(canvas, settings.antiAlias)
	val nativeScene = Scene(nativeEngine)
	var nativeShadow: ShadowGenerator? = null

	override val root: SceneNode = SceneNode()
	private val rootRender: NodeRender
	val nativeCamera = TargetCamera("camera", Vector3.Zero(), nativeScene, true)
	val nativeLight = DirectionalLight("light", Vector3.One(), nativeScene)
	private val supplier = ComponentSupplier(nativeScene)

	override var camera: Camera = BabylonCamera(nativeCamera, { Vector2i(canvas.width, canvas.height) })
	override val nextTimeFuture = supplier.onFrame


	override var environment: Environment = Environment(BLACK.setAlpha(1.0), null, Light(Vector3d.ONE, 1.0), true)
		set(value) {
			field = value
			value.reflectionTexture?.let { reflectionTexture ->
				nativeScene.environmentTexture = supplier.getTexture(reflectionTexture, 0)
			}
			val light = value.light
			if (light != null) {
				if (!nativeScene.lights.contains(nativeLight)) {
					nativeScene.addLight(nativeLight)
				}
				nativeLight.direction = light.direction.normalize().toVector3()
				nativeLight.position = ((-light.direction).normalize() * settings.shadowRange).toVector3()
				nativeLight.intensity = light.intensity
			} else {
				nativeScene.removeLight(nativeLight)
			}
			nativeScene.clearColor = value.backColor.toColor4()

			if (environment.shadow && nativeShadow == null) {
				nativeLight.autoCalcShadowZBounds = true
				nativeLight.autoUpdateExtends = false
				nativeShadow = ShadowGenerator(settings.shadowSize, nativeLight)
				nativeShadow?.bias = 0.01
			}
			if (!environment.shadow && nativeShadow != null) {
				nativeShadow?.dispose()
				nativeShadow = null
			}
		}


	init {
		SceneLoader.ShowLoadingScreen = false
		Inspector(nativeScene)

		nativeScene.useRightHandedSystem = true
		nativeScene.blockMaterialDirtyMechanism = true
//		nativeScene.freezeActiveMeshes()

		rootRender = NodeRender(this, supplier, 0.0, root, null)
		supplier.onFrame.then {
			root.timeLine = root.timeLine.copy(timeOffset = root.timeLine.timeOffset + it)
		}
	}

	override fun runRenderLoop() {
		nativeScene.runRenderLoop()
	}

	companion object {
		fun create(canvasElementId: String, settings: BabylonRenderSettings = BabylonRenderSettings(), blockContextMenu: Boolean = true, autoResize: Boolean = true): BabylonRender {
			val canvas = document.getElementById(canvasElementId)
			if (!(canvas is HTMLCanvasElement)) {
				throw Error("Cant find canvas with id $canvasElementId")
			}

			if (blockContextMenu) {
				canvas.oncontextmenu = {
					false
				}
			}

			val engine = BabylonRender(canvas, settings)
			val scene = engine.nativeScene

			if (autoResize) {
				window.addEventListener("resize", { engine.nativeEngine.resize() })
			}
			return engine
		}
	}
}