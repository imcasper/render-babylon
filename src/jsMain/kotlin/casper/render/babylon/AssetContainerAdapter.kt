package casper.render.babylon

import BABYLON.*
import casper.util.forEachTextureChange

class AssetContainerAdapter(val container: AssetContainer) {
	val textureMap = mutableMapOf<String, BaseTexture>()
	val materialMap = mutableMapOf<String, Material>()
	val geometries = mutableSetOf<Geometry>()
	val nodes = mutableSetOf<TransformNode>()

	init {
		container.textures.forEach {
			textureMap.getOrPut(it.name, { it })
		}
		container.materials.forEach { material ->
			materialMap.getOrPut(material.name, {

				material.forEachTextureChange {
					textureMap[it.name]
							?: throw Error("Not found material by name: ${it.name}")
				}

				material
			})
		}
		container.meshes.forEach {
			val parent = it.parent
			if (parent is TransformNode) {
				nodes += collectAll(parent)
			} else {
				nodes += collectAll(it)
			}
		}
		nodes.forEach {
			if (it is Mesh) {
				it.geometry?.let { geometries.add(it) }
			}
		}
	}

	private fun collectAll(node: TransformNode): List<TransformNode> {
		val additional = mutableListOf<TransformNode>()
		additional += node
		node.getChildren().forEach {
			if (it is TransformNode) {
				additional += it
				additional += collectAll(it)
			}
		}
		return additional
	}
}