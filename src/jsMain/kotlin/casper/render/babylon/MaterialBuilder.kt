package casper.render.babylon

import BABYLON.*
import casper.collection.map.MapUtil
import casper.render.material.*
import casper.render.material.Material

internal object MaterialBuilder {
	fun toNative(scene: Scene, materialReference: MaterialReference, onTexture: (ColorReference, Int) -> BaseTexture): PushMaterial {
		val material = materialReference.data
		if (material.wireFrame) {
			val nativeMaterial = StandardMaterial(materialReference.name ?: "", scene)
			nativeMaterial.zOffset = 1.0
			nativeMaterial.wireframe = true
			nativeMaterial.backFaceCulling = false
			nativeMaterial.disableLighting = true
			nativeMaterial.freeze()
			return nativeMaterial
		} else {
			val nativeMaterial = PBRMaterial(materialReference.name ?: "", scene)
			updateMaterial(nativeMaterial, material, onTexture)
			nativeMaterial.freeze()
			return nativeMaterial
		}
	}

	fun updateMaterial(nativeMaterial: PBRMaterial, material: Material, onTexture: (ColorReference, Int) -> BaseTexture) {
		if (nativeMaterial.metadata == material.hashCode()) return
		nativeMaterial.metadata = material.hashCode()

		toNative(material.roughness) { nativeMaterial.roughness = it }
		toNative(material.metallic) { nativeMaterial.metallic = it }

		inputToNative(material.albedo, 0, onTexture, { nativeMaterial.albedoColor = it }, { nativeMaterial.albedoTexture = it })
		inputToNative(material.ambient, 0, onTexture, { nativeMaterial.ambientColor = it }, { nativeMaterial.ambientTexture = it })
		inputToNative(material.bump, 0, onTexture, {    /*not supported*/ }, { nativeMaterial.bumpTexture = it })
		inputToNative(material.emissive, 0, onTexture, { nativeMaterial.emissiveColor = it }, { nativeMaterial.emissiveTexture = it })
		inputToNative(material.reflection, 0, onTexture, { nativeMaterial.reflectionColor = it }, { nativeMaterial.reflectionTexture = it })

		(material.opacity as? FloatConstantReference)?.let {
			nativeMaterial.alpha = it.data
		}
		(material.opacity as? FloatMapReference)?.let { channel ->
			if (nativeMaterial.opacityTexture == null) {
				val onByte: (Int) -> Byte = { channel.data.getByIndex(it) }
				val map = MapUtil.unionChannel(channel.data.dimension, onByte, onByte, onByte, onByte)
				nativeMaterial.opacityTexture = onTexture(TextureReference(map, "opacity-generated-texture", channel.transform), 1)
			}
		}

		val roughnessChannel = material.roughness as? FloatMapReference
		val metallicChannel = material.metallic as? FloatMapReference
		if (roughnessChannel != null || metallicChannel != null) {
			if (nativeMaterial.metallicTexture == null) {
				val dimension = roughnessChannel?.data?.dimension ?: metallicChannel?.data?.dimension!!

				val onMaxValue: (Int) -> Byte = { 255.toByte() }
				val onMinValue: (Int) -> Byte = { 0.toByte() }
				val onRed: (Int) -> Byte = onMinValue
				val onGreen: (Int) -> Byte = if (roughnessChannel != null) { it -> roughnessChannel.data.array[it] } else onMinValue
				val onBlue: (Int) -> Byte = if (metallicChannel != null) { it -> metallicChannel.data.array[it] } else onMinValue
				val onAlpha: (Int) -> Byte = onMaxValue

				val map = MapUtil.unionChannel(dimension, onRed, onGreen, onBlue, onAlpha)
				val names = mutableListOf<String>()
				roughnessChannel?.name?.let { names.add("rough#$it") }
				metallicChannel?.name?.let { names.add("metal#$it") }


				val transform = metallicChannel?.transform ?: roughnessChannel?.transform
				?: TextureTransform(sampling = Sampling.LINEAR)

				nativeMaterial.metallicTexture = onTexture(TextureReference(map, names.joinToString("+"), transform), 1)
				if (roughnessChannel != null) {
					nativeMaterial.useRoughnessFromMetallicTextureAlpha = false
					nativeMaterial.useRoughnessFromMetallicTextureGreen = true
				}
				if (metallicChannel != null) {
					nativeMaterial.useMetallnessFromMetallicTextureBlue = true
				}
			}
		}
	}

	private fun inputToNative(input: ColorReference?, channelIndex: Int, onTexture: (ColorReference, Int) -> BaseTexture, receiveColor: (Color3) -> Unit, receiveTexture: (BaseTexture) -> Unit) {
		if (input is ColorConstantReference) {
			receiveColor(Color3(input.data.x, input.data.y, input.data.z))
		} else if (input is TextureReference) {
			receiveTexture(onTexture(input, channelIndex))
		}
	}

	fun fromNative(imageMap: Map<BaseTexture, TextureInfo>, nativeMaterial: BABYLON.Material): Material {
		if (nativeMaterial is PBRMaterial) {

			return Material(
					albedo = ChannelBuilder.getAlbedo(imageMap, nativeMaterial),
					roughness = ChannelBuilder.getRoughness(imageMap, nativeMaterial),
					metallic = ChannelBuilder.getMetallic(imageMap, nativeMaterial)
			)
		} else {
			throw Error("Unsupported material: $nativeMaterial")
		}
	}

//	private fun fromNative(value: Number?): FloatReference? {
//		if (value == null) return null
//		return FloatConstantReference(value.toDouble())
//	}

	private fun toNative(value: FloatReference?, onNumber: (Double) -> Unit) {
		if (value is FloatConstantReference) return onNumber(value.data)
	}
}
