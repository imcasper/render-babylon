package casper.render.babylon

import BABYLON.*
import casper.geometry.Quaternion
import casper.geometry.Vector3d
import casper.geometry.interpolateVector
import casper.render.NodeAnimation
import casper.render.NodeAnimationGroup
import casper.render.animation.*
import casper.render.model.SceneNode
import casper.render.model.TimeLine
import casper.util.toQuaternion
import casper.util.toVector3
import casper.util.toVector3d

object AnimationExporter {

	fun buildAnimationGroup(nodeMap:Map<Node, SceneNode>, groupNative: AnimationGroup):NodeAnimationGroup {
		val animationMap = mutableMapOf<SceneNode, MutableList<TransformAnimation<out Any>>>()

		groupNative.targetedAnimations.forEach {animationNative->
			val target = animationNative.target
			val animation = animationNative.animation
			if (target is TransformNode) {
				val node = nodeMap.get(target)
				if (node != null) {
					val nextAnimation = getAnimations(animation)

					animationMap.getOrPut(node) {
						mutableListOf()
					}.add(nextAnimation)
				}
			}
		}

		val animations = animationMap.map {
			NodeAnimation(it.key, TimeLine(animations = it.value))
		}

		return NodeAnimationGroup(groupNative.name, animations)
	}


	fun getAnimations(nativeAnimation: Animation): TransformAnimation<out Any> {
		return if (nativeAnimation.targetProperty == "rotation") {
			RotateAnimation(getQuaternionKeys(nativeAnimation))
		} else if (nativeAnimation.targetProperty == "position") {
			TranslateAnimation(getVectorKeys(nativeAnimation))
		} else if (nativeAnimation.targetProperty == "scaling") {
			ScaleAnimation(getVectorKeys(nativeAnimation))
		} else if (nativeAnimation.targetProperty == "rotationQuaternion") {
			val keys = fromQuaternionKeys(nativeAnimation, nativeAnimation.getKeys().asIterable())
			RotateAnimation(keys)
		} else {
			throw Error("Unsupported animation: ${nativeAnimation.targetProperty}")
		}
	}

	private fun fromQuaternionKeys(animation: Animation, keys: Iterable<IAnimationKey>): List<AnimationKey<Quaternion>> {
		return keys.map {
			val quaternion = it.value as BABYLON.Quaternion
			AnimationKey(it.frame / animation.framePerSecond, quaternion.toQuaternion())
		}
	}

	private fun getQuaternionKeys(animation: Animation): List<AnimationKey<Quaternion>> {
		val keys = animation.getKeys()
		if (keys.size == 2) {
			val first = keys.first()
			val last = keys.last()


			val V = 0.333
			val W = 0.667

			val A = object : IAnimationKey {
				override var frame: Number = first.frame * W + last.frame * V
				override var value: Any = interpolateVector((first.value as Vector3).toVector3d(), (last.value as Vector3).toVector3d(), V).toVector3()
			}

			val B = object : IAnimationKey {
				override var frame: Number = first.frame * V + last.frame * W
				override var value: Any = interpolateVector((first.value as Vector3).toVector3d(), (last.value as Vector3).toVector3d(), W).toVector3()
			}

			return fromEulerKeys(animation, listOf(first, A, B, last))
		} else {
			return fromEulerKeys(animation, animation.getKeys().asIterable())
		}
	}

	private fun fromEulerKeys(animation: Animation, keys: Iterable<IAnimationKey>): List<AnimationKey<Quaternion>> {
		return keys.map {
			val euler = it.value as Vector3
			val value = Quaternion.fromEulerAngles(-euler.toVector3d())
			AnimationKey(it.frame / animation.framePerSecond, value)
		}
	}

	private fun getVectorKeys(animation: Animation): List<AnimationKey<Vector3d>> {
		return animation.getKeys().map {
			val value = it.value as Vector3
			AnimationKey(it.frame / animation.framePerSecond, value.toVector3d())
		}
	}
}