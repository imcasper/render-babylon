package casper.render.babylon

import casper.geometry.Transform
import casper.render.model.TimeLine

internal class NodeAnimator(val startParentTime: Double, val parent: NodeRender?) {
	private var cacheTime = Double.NaN
	private var cacheTransform = Transform.IDENTITY

	fun update(timeLine: TimeLine, transform: Transform):Transform {
		val animations = timeLine.animations
		if (animations.isEmpty()) return transform

		val time = getTime(timeLine)
		if (cacheTime == time) return cacheTransform
		cacheTime = time

		var next = transform
		animations.forEach {
			next = it.execute(next, time, timeLine.animationPlayMode)
		}
		cacheTransform = next
		return next
	}

	fun getTime(timeLine:TimeLine): Double {
		val parentTime = parent?.getTime() ?: 0.0
		val time = timeLine.timeOffset + parentTime * timeLine.timeScale
		return time
	}
}