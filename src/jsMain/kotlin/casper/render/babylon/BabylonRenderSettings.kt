package casper.render.babylon

data class BabylonRenderSettings(val antiAlias: Boolean = true, val shadowSize: Double = 1024.0, val shadowRange:Double =100.0)