package casper.render.babylon

import casper.geometry.Vector2d
import casper.geometry.Vector3d
import casper.geometry.Vector4d
import casper.types.Color4d
import org.khronos.webgl.Float32Array
import org.khronos.webgl.set

internal object FloatArrayUtil {

	fun toVector2d(vertexData: Array<Float>?, vertexIndex: Int): Vector2d? {
		if (vertexData == null) return null
		return Vector2d(
				vertexData.get(vertexIndex * 2 + 0).toDouble(),
				vertexData.get(vertexIndex * 2 + 1).toDouble()
		)
	}

	fun toVector4d(vertexData: Array<Float>?, vertexIndex: Int): Vector4d? {
		if (vertexData == null) return null
		return Vector4d(
				vertexData.get(vertexIndex * 4 + 0).toDouble(),
				vertexData.get(vertexIndex * 4 + 1).toDouble(),
				vertexData.get(vertexIndex * 4 + 2).toDouble(),
				vertexData.get(vertexIndex * 4 + 3).toDouble()
		)
	}

	fun toVector3d(vertexData: Array<Float>?, vertexIndex: Int): Vector3d? {
		if (vertexData == null) return null
		return Vector3d(
				vertexData.get(vertexIndex * 3 + 0).toDouble(),
				vertexData.get(vertexIndex * 3 + 1).toDouble(),
				vertexData.get(vertexIndex * 3 + 2).toDouble()
		)
	}

	fun fromVector2d(value: Vector2d?, vertexIndex: Int, getBuffer:()->Float32Array) {
		if (value == null) return
		val buffer = getBuffer()

		buffer[vertexIndex * 2 + 0] = value.x.toFloat()
		buffer[vertexIndex * 2 + 1] = value.y.toFloat()
	}

	fun fromVector3d(value: Vector3d?, vertexIndex:Int, getBuffer:()->Float32Array) {
		if (value == null) return
		val buffer = getBuffer()

		buffer.set(vertexIndex * 3 + 0, value.x.toFloat())
		buffer.set(vertexIndex * 3 + 1, value.y.toFloat())
		buffer.set(vertexIndex * 3 + 2, value.z.toFloat())
	}

	fun fromVector4dToColor(value: Color4d?, vertexIndex: Int, getBuffer:()->Float32Array) {
		if (value == null) return
		val buffer = getBuffer()

		buffer[vertexIndex * 4 + 0] = value.x.toFloat()
		buffer[vertexIndex * 4 + 1] = value.y.toFloat()
		buffer[vertexIndex * 4 + 2] = value.z.toFloat()
		buffer[vertexIndex * 4 + 3] = value.w.toFloat()
	}

}