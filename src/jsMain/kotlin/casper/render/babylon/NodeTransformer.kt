package casper.render.babylon

import BABYLON.*
import casper.geometry.Matrix4d
import casper.geometry.basis.Box3d
import casper.render.extension.BoundingBox
import casper.render.model.Mesh
import casper.util.addMeshToScene
import casper.util.removeMeshFromScene
import kotlin.random.Random

internal class NodeTransformer() {
	private val nativeWorldMatrix = Matrix()
	private var cacheBox: Box3d? = null

	fun updateMatrix(nativeNode: TransformNode, worldMatrix: Matrix4d) {
		val floatList = worldMatrix.data.map { it.toFloat() }.toMutableList()
		nativeWorldMatrix.m.set(floatList.toTypedArray())
		applyMatrix(nativeNode)
		updateBoundingInfo(nativeNode, worldMatrix[12], worldMatrix[13], worldMatrix[14])
	}

	private fun applyMatrix(nativeNode: TransformNode) {
		nativeNode.freezeWorldMatrix(nativeWorldMatrix)
	}

	private fun updateBoundingInfo(nativeNode: TransformNode, x: Double, y: Double, z: Double) {
		val mesh = (nativeNode as? InstancedMesh) ?: return
		val box = cacheBox
		if (box == null) {
			mesh.isVisible = false
			return
		} else {
			mesh.isVisible = true
		}
		val min = box.min
		val max = box.max
		mesh.setBoundingInfo(BoundingInfo(Vector3(x + min.x, y + min.y, z + min.z), Vector3(x + max.x, y + max.y, z + max.z)))
	}

	fun updateBox(nextMesh: Mesh?) {
		val data = nextMesh?.vertices?.data
		if (data != null && data.isNotEmpty()) {
			cacheBox = BoundingBox.calculate(data)
		} else {
			cacheBox = null
		}
	}
}