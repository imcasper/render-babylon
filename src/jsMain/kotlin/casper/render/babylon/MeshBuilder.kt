package casper.render.babylon

import BABYLON.AbstractMesh
import BABYLON.InstancedMesh
import casper.render.model.Mesh
import casper.util.removeMeshFromScene

internal object MeshBuilder {
	val USE_INDICES = false

	fun createOriginal(supplier: ComponentSupplier, model: Mesh): BABYLON.Mesh {
		val vertices = model.vertices

		val mesh = BABYLON.Mesh(model.name ?: "")
		mesh.material = supplier.getMaterial(model.material)

		mesh.isPickable = false
		mesh.isUnIndexed = !USE_INDICES

		val hasColors = (vertices.data.firstOrNull()?.color != null)
		mesh.useVertexColors = hasColors

		supplier.getGeometry(vertices)?.applyToMesh(mesh)

		mesh.freezeWorldMatrix()
		mesh.removeMeshFromScene()

		return mesh
	}

	fun createInstance(originalMesh: BABYLON.Mesh): InstancedMesh {
		val node = InstancedMesh("", originalMesh)
		node.isPickable = false

		node.doNotSyncBoundingInfo = true
		node.cullingStrategy = AbstractMesh.CULLINGSTRATEGY_BOUNDINGSPHERE_ONLY
		return node
	}
}