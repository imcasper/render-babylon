package casper.render.babylon

import BABYLON.BaseTexture
import BABYLON.PBRMaterial
import BABYLON.Texture
import casper.collection.map.IntMap2d
import casper.collection.map.MapUtil
import casper.render.material.*
import casper.types.Color4d

internal object ChannelBuilder {
	fun getAlbedo(imageMap: Map<BaseTexture, TextureInfo>, nativeMaterial: PBRMaterial): ColorReference? {
		var albedo: ColorReference? = null

		nativeMaterial.albedoColor.let {
			albedo = ColorConstantReference(Color4d(it.r, it.g, it.b, 1.0))
		}

		nativeMaterial.albedoTexture?.let {
			val colors = imageMap.get(it)
			if (colors != null) {
				albedo = TextureReference(colors.map, it.name, TextureTransform(sampling = colors.sampling))
			}
		}

		return albedo
	}

	fun getRoughness(imageMap: Map<BaseTexture, TextureInfo>, nativeMaterial: PBRMaterial): FloatReference? {
		var roughness: FloatReference? = null

		nativeMaterial.roughness?.let {
			roughness = FloatConstantReference( it)
		}

		nativeMaterial.metallicTexture?.let {
			val colors = imageMap.get(it)
			if (colors != null) {
				if (nativeMaterial.useRoughnessFromMetallicTextureAlpha) {
					roughness = FloatMapReference(MapUtil.takeChannel(colors.map, 3), createChannelName(it.name, 3), TextureTransform(sampling = colors.sampling))
				} else if (nativeMaterial.useRoughnessFromMetallicTextureGreen) {
					roughness = FloatMapReference(MapUtil.takeChannel(colors.map, 1), createChannelName(it.name, 1), TextureTransform(sampling = colors.sampling))
				}
			}
		}

		return roughness
	}

	private fun createChannelName(name: String, i: Int): String {
		return when (i) {
			0 -> "$name#red"
			1 -> "$name#green"
			2 -> "$name#blue"
			3 -> "$name#alpha"
			else -> "$name#$i"
		}
	}

	fun getMetallic(imageMap: Map<BaseTexture, TextureInfo>, nativeMaterial: PBRMaterial): FloatReference? {
		var metallic: FloatReference? = null

		nativeMaterial.metallic?.let {
			metallic = FloatConstantReference( it)
		}

		nativeMaterial.metallicTexture?.let {
			val colors = imageMap.get(it)
			if (colors != null) {
				val channelIndex = if (nativeMaterial.useMetallnessFromMetallicTextureBlue) 2 else 0
				val channel = MapUtil.takeChannel(colors.map, channelIndex)
				metallic = FloatMapReference(channel, createChannelName(it.name, channelIndex), TextureTransform(sampling = colors.sampling))
			}
		}

		return metallic
	}
}