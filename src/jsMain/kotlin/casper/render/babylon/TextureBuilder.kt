package casper.render.babylon

import BABYLON.*
import casper.collection.map.IntMap2d
import casper.render.material.ColorReference
import casper.render.material.CubeTextureReference
import casper.render.material.Sampling
import casper.render.material.TextureReference
import casper.types.ColorUtil
import org.khronos.webgl.ArrayBufferView
import org.khronos.webgl.Float32Array
import org.khronos.webgl.Int32Array
import org.khronos.webgl.Uint8Array

internal object TextureBuilder {
	fun toNative(scene: Scene, source: ColorReference, channelIndex: Int): BaseTexture {
		val texture = if (source is TextureReference) {
			val map = source.data
			val name = source.name
			val array = map.array as Int32Array

			val bytes = Uint8Array(array.buffer)
			val sampling = when (source.transform.sampling) {
				Sampling.NEAREST -> Constants.TEXTURE_NEAREST_NEAREST_MIPLINEAR
				Sampling.LINEAR -> Constants.TEXTURE_LINEAR_LINEAR_MIPLINEAR
			}
			val texture = RawTexture(bytes, map.width.toDouble(), map.height.toDouble(), Constants.TEXTUREFORMAT_RGBA.toDouble(), scene, true, samplingMode = sampling.toDouble())
			if (name != null) {
				texture.name = name
			}
			texture
		} else if (source is CubeTextureReference) {
			val data = source.data
			val name = source.name
			val converter: (IntMap2d) -> ArrayBufferView = ::asJsBytes
			val size = data.nx.width
			val items = arrayOf(converter(data.nx), converter(data.px), converter(data.ny), converter(data.py), converter(data.nz), converter(data.pz))

			val texture = RawCubeTexture(scene, items, size.toDouble(), format = Constants.TEXTUREFORMAT_RGBA.toDouble(), invertY = false, generateMipMaps = true, type = Constants.TEXTURETYPE_UNSIGNED_BYTE.toDouble())
			//	todo: i dont now what this... but it need for correct work environment
			texture.lodGenerationScale = 0.8
			if (name != null) {
				texture.name = name
			}
			texture
		} else {
			throw Error("Unsupported format")
		}

		texture.coordinatesIndex = channelIndex
		texture.wrapU = Constants.TEXTURE_WRAP_ADDRESSMODE.toDouble()
		texture.wrapV = Constants.TEXTURE_WRAP_ADDRESSMODE.toDouble()
		texture.wrapR = Constants.TEXTURE_WRAP_ADDRESSMODE.toDouble()
		return texture
	}

	fun asJsBytes(map: IntMap2d): Uint8Array {
		return Uint8Array((map.array as Int32Array).buffer)
	}

	fun colorAs4Float(map: IntMap2d): Float32Array {
		val size = map.width * map.height * 4
		val float = Array<Float>(size) {
			val source = map.array[it / 4]
			var red = ColorUtil.decodeComponentAsDouble(it % 4, source)
			if (it % 4 != 3) red *= 1.0
			red.toFloat()
		}
		val f = Float32Array(float)
		return f
	}

}