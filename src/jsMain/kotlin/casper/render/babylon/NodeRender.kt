package casper.render.babylon

import BABYLON.AbstractMesh
import BABYLON.InstancedMesh
import BABYLON.TransformNode
import casper.core.DisposableHolder
import casper.geometry.Matrix4d
import casper.geometry.Transform
import casper.render.model.Mesh
import casper.render.model.SceneNode
import casper.signal.concrete.Slot

internal class NodeRender(val render: BabylonRender, val supplier: ComponentSupplier, startParentTime: Double, private val sceneNode: SceneNode, private val parent: NodeRender?) : DisposableHolder() {
	private var nativeNode = TransformNode(getName(), supplier.scene)

	private val renders = mutableMapOf<SceneNode, NodeRender>()

	private val animator = NodeAnimator(startParentTime, parent)
	private val transformer = NodeTransformer()

	private var cacheMesh: Mesh? = null

	private var cacheChildren:Set<SceneNode>? = null
	private var localTransform = Transform.IDENTITY
	private var cacheWorldMatrix: Matrix4d? = null

	private var onNextTimeSlot: Slot? = null
	private var onChangedSlot: Slot? = null
	private var onInvalidateSlot: Slot? = null

	init {
		checkParent(this)
		update()
	}

	private fun checkParent(start:NodeRender) {
		if (parent == start) throw Error("Invalid parent for $start")
		if (parent == null) return
		parent.checkParent(start)
	}

	override fun dispose() {
		super.dispose()
		parent?.renders?.remove(sceneNode)
		renders.values.forEach {
			it.dispose()
		}
		renders.clear()
		nativeNode.dispose()
		onNextTimeSlot?.dispose()
		onNextTimeSlot = null
		onChangedSlot?.dispose()
		onChangedSlot = null
		onInvalidateSlot?.dispose()
		onInvalidateSlot = null
	}

	private fun updateObservers() {
		val hasAnimation = sceneNode.timeLine.animations.isNotEmpty()
		if (hasAnimation) {
			if (onNextTimeSlot == null) {
				onNextTimeSlot = render.nextTimeFuture.then {
					update()
				}
			}
			onChangedSlot?.dispose()
			onChangedSlot = null
			onInvalidateSlot?.dispose()
			onInvalidateSlot = null
		} else {
			if (onChangedSlot == null) {
				onChangedSlot = sceneNode.changed.then {
//					update()
					if (onInvalidateSlot == null) {
						onInvalidateSlot = render.nextTimeFuture.then {
							update()
							onInvalidateSlot?.dispose()
							onInvalidateSlot = null
						}
					}
				}
			}
			onNextTimeSlot?.dispose()
			onNextTimeSlot = null
		}
	}

	private fun update() {
		if (isDisposed) return

		updateMesh()
		updateTransform()
		updateChildren()

		updateObservers()
	}

	private fun getName(): String {
		val content = sceneNode.model
		return content.name ?: content.name ?: ""
	}

	private fun updateMesh() {
		val nextMesh = sceneNode.model.mesh
		if (cacheMesh === nextMesh) return

		cacheMesh = nextMesh
		transformer.updateBox(nextMesh)
		nativeNode.dispose()
		(nativeNode as? InstancedMesh)?.let {
			updateShadow(it, false)
		}
		nativeNode = createNode(nextMesh)
	}

	private fun createNode(mesh: Mesh?): TransformNode {
		if (isDisposed)
			throw Error("Work with disposed NodeRender")

		val node = if (mesh == null) {
			return TransformNode("", supplier.scene)
		} else {
			val source = supplier.getMesh(mesh)
			if (source.geometry == null) {
				source
			} else {
				val node = MeshBuilder.createInstance(source)
				updateShadow(node, render.environment.shadow)
				node
			}
		}
		node.name = getName()
		transformer.updateMatrix(node, getWorldMatrix())
		return node
	}

	private fun updateShadow(node: InstancedMesh, show: Boolean) {
		render.nativeShadow?.let {
			if (show) {
				node.sourceMesh.receiveShadows = show
				it.addShadowCaster(node)
			} else {
				it.removeShadowCaster(node)
			}
		}
	}

	private fun getWorldMatrix(): Matrix4d {
		cacheWorldMatrix?.let {
			return it
		}
		val localMatrix = localTransform.getMatrix()
		val worldMatrix = if (parent == null) {
			localMatrix
		} else {
			localMatrix * parent.getWorldMatrix()
		}
		cacheWorldMatrix = worldMatrix
		return worldMatrix
	}

	private fun updateTransform() {
		val currentTransform = animator.update(sceneNode.timeLine, sceneNode.transform)

		if (localTransform !== currentTransform || cacheWorldMatrix == null) {
			localTransform = currentTransform
			clearWorldMatrixCache()
			transformer.updateMatrix(nativeNode, getWorldMatrix())

			renders.values.forEach {
				it.updateTransform()
			}
		}
	}

	private fun clearWorldMatrixCache() {
		if (cacheWorldMatrix == null) return
		cacheWorldMatrix = null
		renders.values.forEach {
			it.clearWorldMatrixCache()
		}
	}

	fun getTime(): Double {
		return animator.getTime(sceneNode.timeLine)
	}

	private fun updateChildren() {
		val children = sceneNode.model.children

		if (cacheChildren === children) return
		cacheChildren = children

		renders.filter {
			!children.contains(it.key)
		}.values.forEach {
			it.dispose()
		}

		children.forEach { childNode ->
			renders.getOrPut(childNode) {
				NodeRender(render, supplier, getTime(), childNode, this)
			}
		}
	}
}