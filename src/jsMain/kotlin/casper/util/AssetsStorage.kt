package casper.util

import BABYLON.Scene
import casper.asset.AssetFuture
import casper.asset.TypedAssetCollection
import casper.asset.TypedAssetLoaderCollection
import casper.loader.BitmapReference
import casper.loader.createBitmapLoader
import casper.render.SceneData
import casper.signal.concrete.EitherFuture
import casper.signal.concrete.EitherPromise
import casper.types.Bitmap
import casper.util.atlas.Atlas
import casper.util.loader.createAtlasLoader

class AssetsStorage(val scene: Scene) {
	val scenes = TypedAssetCollection(TypedAssetLoaderCollection { fileName -> attachLog(createSceneLoader(scene, fileName), {it.name}) })
	val bitmaps = TypedAssetCollection(TypedAssetLoaderCollection { fileName -> attachLog(createBitmapLoader(fileName), {it.name}) })
	val atlases = TypedAssetCollection(TypedAssetLoaderCollection { fileName -> attachLog(createAtlasLoader(fileName), {it.name}) })
	val customs = mutableMapOf<String, Any>()

	inline fun <reified Custom : Any> getCustom(key: String, op: () -> Custom): Custom {
		val lastValue = customs.get(key)
		if (lastValue == null || lastValue !is Custom) {
			val nextValue = op()
			customs[key] = nextValue
			return nextValue
		}
		return lastValue
	}

	private fun <A, String> attachLog(promise: EitherFuture<A, String>, getName:(A)->String): EitherFuture<A, String> {
		promise.thenAccept {
			println("${getName(it)} is loaded")
		}
		promise.thenReject {
			println(it)
		}
		return promise
	}

	fun getSceneFuture(fileName: String): AssetFuture<SceneData> {
		return scenes.loader(fileName)
	}

	fun getBitmapFuture(fileName: String): AssetFuture<BitmapReference> {
		return bitmaps.loader(fileName)
	}

	fun getAtlasFuture(fileName: String): AssetFuture<Atlas> {
		return atlases.loader(fileName)
	}

	fun getContent(name: String): SceneData? {
		return scenes.get(name)
	}

	fun getBitmap(name: String): BitmapReference? {
		return bitmaps.get(name)
	}

	fun getAtlas(name: String): Atlas? {
		return atlases.get(name)
	}
}